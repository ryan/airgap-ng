# Airgap NG

A slim version of Arch Linux intended to run on airgapped (always-offline)
systems. The builder runs in Docker using privileged mode.

## Target Packages

Airgap NG is built for the purpose of providing an airgapped image of Keyfork.
As such, it builds an Arch package for Keyfork and a local Arch package
repository from which to install Keyfork. The base installation image will also
include Nettle, PCSC Lite

## Building

Requirements:

* `docker`
* `make`

The default target is the `airgap` image, but a different one can be specified.

```sh
config="airgap"
make "out/archlinux-baseline-arch-${config}-x86_64.iso"
```

## Flashing

Assuming your block device is `/dev/sdb`:

```sh
pv < out/archlinux-baseline-arch-airgap-x86_64.iso | sudo dd of=/dev/sdb
```

Alternatively, if `pv` is not installed, the following can be run:

```sh
sudo dd if=out/archlinux-baseline-arch-airgap-x86_64.iso of=/dev/sdb
```

## Running

Once the ISO image has been flashed to a USB drive, the drive can be used to
boot Airgap Arch Linux. The following instructions have been tested on a Lenovo
system but may be applicable to other systems:

Power on the device, and spam the F2 key. This will open the BIOS Setup
Utility. In the BIOS Setup Utility, the "Secure Boot" option (often under a
submenu "Boot") should be disabled. Save the settings (typically F10) and
reboot, spamming the F12 key to open the EFI boot menu. Select the EFI USB
Device option to boot Airgap Arch Linux. The default terminal should be
automatically logged in as root. Once Airgap Arch Linux has been started, the
boot drive may optionally be removed.

### Using Keyfork: Mounting a Shardfile

This guide assumes an SD card is plugged in with a `shards.pgp` file. The SD
card should be an unmounted card, named something like `mmcblk1p1`, with a
single partition. There may be a similar device with multiple partitions, but
that is the boot drive, and most likely does not contain the shard file. The
shard partition may be mounted using:

```
% lsblk
NAME         MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
loop0          7:0    0 404.8M  1 loop /run/archiso/airootfs
sda            8:0    1  57.8G  0 disk 
├─sda1         8:1    1   466M  0 part 
└─sda2         8:2    1    13M  0 part 
mmcblk0      179:0    0  58.2G  0 disk 
├─mmcblk0p1  179:1    0   260M  0 part 
├─mmcblk0p2  179:2    0    16M  0 part 
├─mmcblk0p3  179:3    0    56G  0 part 
└─mmcblk0p4  179:4    0     2G  0 part 
mmcblk0boot0 179:8    0     4M  1 disk 
mmcblk0boot1 179:16   0     4M  1 disk 
mmcblk1      179:24   0  29.7G  0 disk 
└─mmcblk1p1  179:25   0  29.7G  0 part 
% mount /dev/mmcblk1p1 /mnt
```

### Using Keyfork: Scanning QR Codes

When running `keyfork recover remote-shard` or `keyfork shard transport`,
Keyfork may prompt for a QR code to be scanned. The QR code can be a screenshot
but should not be skewed (it should show up square on the phone) and should
take up the width of the phone's screen. The phone should be held between two
to three inches from the camera, as holding it further away may lead to a
"light bleeding" effect causing the QR code to become unreadable. The phone
should be held as still as possible to avoid incorporating motion blur.
