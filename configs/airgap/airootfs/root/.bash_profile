help() {
cat <<EOF
Airgap Arch Linux with Keyfork is best used with a removable SD card to store
shard files and other persistent media. After inserting an SD card, run the
following command to mount any unmounted SD cards and find any Shard files:

  # discover-shard

This guide will assume the discovered Shard file is listed as:
  /media/mmcblk0p1/shards.pgp
If an alternative Shard file is found, that file should be used instead.

Keyfork can be used to set up a Shard-secured secret with an M-of-N scheme,
where M is the minimum amount of users required to recreate the secret and N is
is the total amount of shardholders. An additional value, K, will be used to
denote the amount of smartcards (i.e., backups) to provision per shardholder.

  # prepare-shard-media
  # keyfork wizard generate-shard-secret --threshold \$M --max \$N
    --keys-per-shard \$K --output /media/sdcard/shards.pgp

The Keyfork server can be started using a Shard-secred secret locally, if all
shardholders are present, or using a QR-based remote recovery. The shard file
does not have to be present for remote recovery.

  # keyfork recover shard /media/mmcblk0p1/shards.pgp

Or

  # keyfork recover remote-shard

If performing a "Remote Shard" operation, Keyfork shards can be transported to
the system performing the operation by running the following command:

  # keyfork shard transport /media/mmcblk0p1/shards.pgp

For more information, run:

  # keyfork help
EOF
}

amount() {
  partition="$1"
  media="$1"
  if [ ! -z "$2" ]; then
    media="$2"
  fi
  echo "Automatically mounting /dev/$partition to /media/$media"
  mkdir -p "/media/$media"
  umount "/media/$partition" 2>/dev/null
  mount $MOUNT_OPTS "/dev/$partition" "/media/$media"
}

discover-shard() {
  MOUNT_OPTS="-o relatime,utf8,flush,umask=0000"

  lsblk -nlo NAME,FSTYPE | awk '$2 == "vfat" { print $1 }' | while read partition; do
    amount "$partition"
    find "/media/$partition" -name '*.pgp' -maxdepth 2 | while read file; do
      echo "Found potential shardfile: $file"
    done
  done
}

prepare-shard-media() {
  echo "Please re-insert the SD card"
  udevadm monitor --udev | while read udev ts operation path type; do
    if [ "$type" = "(block)" -a "$operation" = "add" ]; then
      partition="$(echo "$path" | cut -d/ -f12)"
      if [ ! -z "$partition" ]; then
        amount "$partition" "sdcard"
        # Weird quirk with bash: if udev doesn't have anything else printing,
        # it won't trigger the start of the next loop, and therefore won't
        # "break" the loop. Run `udevadm trigger` to give udevadm more things
        # to send to `read`.
        udevadm trigger
        break
      fi
    fi
  done
}

help
echo
discover-shard
echo
