FROM archlinux:base-devel AS keyfork-build

RUN pacman -Syu --noconfirm
RUN useradd -m build && echo "build ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/build

USER build
WORKDIR /home/build
ADD PKGBUILD.keyfork /home/build/PKGBUILD

RUN makepkg -s --noconfirm --skipinteg
RUN mkdir /home/build/repo && cp keyfork-0.0.0-1-x86_64.pkg.tar.zst repo && repo-add /home/build/repo/keyfork.db.tar.gz keyfork-0.0.0-1-x86_64.pkg.tar.zst

FROM archlinux

RUN pacman -Syu --noconfirm archiso
ADD configs /configs
COPY --from=keyfork-build /home/build/repo /usr/share/keyfork
WORKDIR /

ENTRYPOINT ["/usr/bin/bash", "-x", "/usr/bin/mkarchiso", "-v", "-w", "/work", "-o", "/out"]
CMD "/usr/share/archiso/configs/baseline"
