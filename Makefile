# Known configs:
# * airgap

CONFIG ?= airgap

.PHONY: default
default: out/archlinux-baseline-arch-$(CONFIG)-x86_64.iso

.PHONY: vars
vars:
	@echo "CONFIG ?= $(CONFIG)"

.PHONY: clean
clean:
	rm -rf out work || true

# NOTE: Relies on files in configs/% but make doesn't have recursive checks
out/archlinux-baseline-arch-%-x86_64.iso: configs/% Dockerfile PKGBUILD.keyfork
	$(MAKE) docker-airgap-builder
	mkdir -p out
	docker run --rm --privileged --volume "$(PWD)/out:/out" airgap-builder $<

.PHONY: docker-airgap-builder
docker-airgap-builder:
	docker build -t airgap-builder .
